package project.testitext;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;


import com.itextpdf.tool.xml.pipeline.css.CSSResolver;





/**
 * Hello world!
 *
 */
public class App {
	private static final String CSS = "csstest.css";
	private static final String HTML = "index.html";
	private static final String DEST = "pdfFonts.pdf";

	public static void main(String... args) {

		try {
			Document document = new Document(PageSize.LETTER);
			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(DEST));
			document.open();
	
			//Font
			XMLWorkerFontProvider fontImp = new XMLWorkerFontProvider();
			fontImp.register("font.ttf");

			//CSS
			CSSResolver cssResolver = new StyleAttrCSSResolver();
			CssFile cssFile = XMLWorkerHelper.getCSS(new FileInputStream(CSS));
			cssResolver.addCss(cssFile);

			
			XMLWorkerHelper.getInstance().parseXHtml(pdfWriter, document, new FileInputStream(HTML),
					new FileInputStream(CSS), Charset.forName("UTF-8"), fontImp);

			document.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}


